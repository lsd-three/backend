# Backend for Faraday Car Rental application
<h3 align="right">Group <em>‘Three’</em> in LSD course</h3> 

</br>


This repository contains a maintained version of the core logic for **Faraday Car Rental** application - a project we needed to develop for being the grounds of our <em>Large System Development</em> course exam. We have used Java as our programming language.

The way we established the connection with the Frontend is through RPC technology - RMI is the specific implementation for Java.

Apart from the business logic, we have implemented Unit and Integrations testing.

For our integration testing, we are using an embedded SQL database - H2. This way we optimize the testing experience by making the tests’ execution faster, unlike our observations when connecting to a running MySQL instance.
Furthermore, we always initialize the database with the same values which results in the tests becoming more reliable.

We are also using an ORM EclipseLink for Java with JDBC (MySQL driver).


</br>
</br>

___
> #### Project made by:   
`Alexander Winther` [Github](https://github.com/awha86) <br/>
`David Alves` [Github](https://github.com/davi7725) <br />
`Elitsa Marinovska` [Github](https://github.com/elit0451) <br />
`Mathias Bigler` [Github](https://github.com/Zurina) <br />
`Stanislav Novitski` [Github](https://github.com/Stani2980) <br />
> Attending "Large Systems Development" course of Software Development bachelor's degree