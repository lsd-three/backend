/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContractImpl;

import DTOs.AvailabilityDetails;
import DTOs.CarDetails;
import Database.CarDBFacade;
import exceptions.GetAvailableCarsException;
import interfaces.ICars;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author stanislavnovitski
 */
public class CarsImpl extends UnicastRemoteObject implements Serializable, ICars {

    CarDBFacade carDB;

    public CarsImpl() throws RemoteException {
        super();
        carDB = new CarDBFacade();
    }

    @Override
    public List<CarDetails> getAvailableCars(AvailabilityDetails availability) throws GetAvailableCarsException, RemoteException {

        List cars = carDB.getAvailableCars(availability);

        return cars;
    }

}
