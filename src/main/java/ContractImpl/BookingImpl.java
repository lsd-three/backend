/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContractImpl;

import DTOs.BookingDetails;
import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.NewBookingDetails;
import Database.BookingDBFacade;
import exceptions.AddNewBookingException;
import exceptions.GetSpecificBookingException;
import exceptions.SearchBookingException;
import interfaces.IBooking;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author stanislavnovitski
 */
public class BookingImpl extends UnicastRemoteObject implements Serializable, IBooking {

    public BookingImpl() throws RemoteException {
        super();
    }

    BookingDBFacade bf = new BookingDBFacade();

    @Override
    public BookingDetails addNewBooking(NewBookingDetails newBookingDetails) throws AddNewBookingException, RemoteException {
        return bf.addNewBooking(newBookingDetails);
    }

    @Override
    public List<BookingDetails> searchBooking(DriverIdentifier driverId) throws SearchBookingException, RemoteException {
        return bf.searchBooking(driverId);
    }

    @Override
    public BookingDetails getSpecificBooking(BookingIdentifier bookingId) throws GetSpecificBookingException, RemoteException {
        return bf.getSpecificBooking(bookingId);
    }

    @Override
    public void cancelBooking(BookingIdentifier bookingId) throws RemoteException {
        bf.cancelBooking(bookingId);
    }
}
