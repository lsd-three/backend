package com.threecarrental.main;

import ContractImpl.BookingImpl;
import ContractImpl.CarsImpl;
import Database.EntityManagerSingleton;
import Endpoint.PingEndpoint;
import interfaces.IBooking;
import interfaces.ICars;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    public static Registry registry;
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public Main() throws RemoteException {
    }

    public static void main(String[] args) {
        // instantiate EnitityManager
        EntityManagerSingleton ems = new EntityManagerSingleton();
        PingEndpoint.createEndpoint();
        createRegistry();
    }

    /**
     * Creates the two RMI services we provide One for Booking methods The
     * second for Car methods These are implementations of the Contract
     * interfaces.
     */
    private static void createRegistry() {
        try {
            // This was necessary to make it exposed to other things than localhost
            System.setProperty("java.rmi.server.hostname", "46.101.241.48");
            System.out.println("RMI server localhost starts");

            // Get a server registry at default port 1099
            registry = LocateRegistry.createRegistry(1099);
            System.out.println("RMI registry created ");

            // Create engines of remote services, running on the server
            IBooking bookingRemoteEngine = (IBooking) new BookingImpl();
            ICars carsRemoteEngine = (ICars) new CarsImpl();

            // Register booking engine 
            String bookingEngineName = "Booking";
            Naming.rebind(bookingEngineName, bookingRemoteEngine);
            System.out.println("Engine " + bookingEngineName + " bound in registry");

            // Register booking engine 
            String carsEngineName = "Cars";
            Naming.rebind(carsEngineName, carsRemoteEngine);
            System.out.println("Engine " + carsEngineName + " bound in registry");

        } catch (Exception e) {
            System.err.println("Exception: " + e);
            LOGGER.log(Level.FATAL, "Exception: "+e+"\n"+Arrays.toString(e.getStackTrace()));
        }
    }

}
