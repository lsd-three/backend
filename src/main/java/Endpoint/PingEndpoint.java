/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endpoint;

import static spark.Spark.get;
 
public class PingEndpoint {
    
    public static void createEndpoint() {
        get("/", (request, response) -> "1");
    }
    
}
