/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import DTOs.AirportDetails;
import DTOs.BookingDetails;
import DTOs.CarDetails;
import DTOs.CarTypeDetails;
import DTOs.DriverDetails;
import DTOs.HotelDetails;
import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.Identifiers.StationIdentifier;
import DTOs.NewBookingDetails;
import Entities.Booking;
import Entities.Car;
import Entities.CarType;
import Entities.Rentee;
import Entities.Station;
import exceptions.AddNewBookingException;
import exceptions.GetSpecificBookingException;
import exceptions.SearchBookingException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BookingDBFacade {

    EntityManager em;
    private static final Logger LOGGER = LogManager.getLogger(BookingDBFacade.class);

    public BookingDBFacade() {
        em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
    }

    public BookingDBFacade(EntityManager entityManager) {
        em = entityManager;
    }

    public BookingDetails addNewBooking(NewBookingDetails newBookingDetails) throws AddNewBookingException, RemoteException {
        LOGGER.log(Level.INFO, "AddNewBooking method was called");
        // A check to see if the EntityManager has timed out - if it has, we instantiate a new one.
        if (!em.isOpen()) {
            em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
        }

        if (newBookingDetails == null) {
            LOGGER.log(Level.WARN, "The provided format was either null or malformed");
            throw new AddNewBookingException("The provided format was either null or malformed");
        }

        BookingDetails bookingDetails = null;
        EntityTransaction transaction = null;
        Booking booking = null;
        try {
            transaction = em.getTransaction();
            booking = new Booking(
                    1,
                    newBookingDetails.getPickUpTime(),
                    newBookingDetails.getDeliveryTime()
            );

            // Handle if the driverId is not provided in the bookingDetails
            Integer driverId = Integer.MAX_VALUE;;
            try {
                driverId = Integer.parseInt(newBookingDetails.getDriver().getId());
            } catch (Exception ex) {
                LOGGER.log(Level.WARN, "DriverId was not present", ex);
            }

            // Get driver from DB or get null
            Rentee rentee = em.find(Rentee.class, driverId);

            // If it is a non existing rentee/driver
            if (rentee == null) {
                DriverDetails d = newBookingDetails.getDriver();
                Rentee newRentee = new Rentee(null, d.getName(), d.getAge(), d.getDriverLicenseNumber());
                transaction.begin();
                em.persist(newRentee);
                em.flush();
                transaction.commit();
                rentee = em.find(Rentee.class, newRentee.getRenteeId());
                booking.setRentee(rentee);
            } else {
                booking.setRentee(rentee);
            }

            Car c = em.find(Car.class, newBookingDetails.getCar().getId());
            booking.setCar(c);

            Station pickup = em.find(Station.class, Integer.parseInt(newBookingDetails.getPickUpPlace().getId()));
            Station delivery = em.find(Station.class, Integer.parseInt(newBookingDetails.getDeliveryPlace().getId()));

            booking.setPickupPlace(pickup);
            booking.setDeliveryPlace(delivery);

            // Calculate Totalprice 
            double totalPrice = calcTotalPrice(c.getCarType(), newBookingDetails.getPickUpTime(), newBookingDetails.getDeliveryTime(), pickup, delivery);

            booking.setTotalPrice(new BigDecimal(totalPrice));

            transaction.begin();
            em.persist(booking);
            transaction.commit();

            bookingDetails = new BookingDetails(
                    newBookingDetails.getPickUpTime(),
                    newBookingDetails.getDeliveryTime(),
                    newBookingDetails.getPickUpPlace(),
                    newBookingDetails.getDeliveryPlace(),
                    new DriverDetails(
                            rentee.getRenteeId() + "",
                            rentee.getName(),
                            rentee.getDateOfBirth(),
                            rentee.getLicenseNumber()),
                    newBookingDetails.getCar(),
                    totalPrice,
                    booking.getBookingId().toString());
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.log(Level.ERROR, "There occured an internal server error while trying to delete the specific booking: " + ex + "\n" + Arrays.toString(ex.getStackTrace()));
            throw new AddNewBookingException("There occured an error while trying to add the booking.");
        }

        return bookingDetails;
    }

    /**
     * Method to calculate days of rental and to add an extra fee if the pickup
     * place and delivery do not match. the extra fee is just set to 50.00
     *
     * @param ct cartype that contains the price per day for the chosen car
     * @param pickupTime pickup time
     * @param deliveryTime delivery time
     * @param pickupPlace Station where the car is picked up
     * @param deliveryPlace Station where it is returned
     * @return
     */
    public double calcTotalPrice(CarType ct, Date pickupTime, Date deliveryTime, Station pickupPlace, Station deliveryPlace) {
        double res = 0.00;

        // Calculate difference between rental days.
        long diff = deliveryTime.getTime() - pickupTime.getTime();

        // Add a day because the rented day counts as a full day
        diff += 86400000; // 86 400 000 milli seconds = 1 day

        int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        res = ct.getPrice().doubleValue() * Double.valueOf(days);

        // If the pickup and delivery place are diffrent add 50.00 to the price
        if (pickupPlace.getStationId() != deliveryPlace.getStationId()) {
            res += 50.00;
        }
        return res;
    }

    public List<BookingDetails> searchBooking(DriverIdentifier driverId) throws SearchBookingException, RemoteException {
        LOGGER.log(Level.INFO, "SearchBooking method was called");
        // A check to see if the EntityManager has timed out - if it has, we instantiate a new one.
        if (!em.isOpen()) {
            em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
        }
        List<BookingDetails> bookingList = new ArrayList<>();

        try {
            Rentee rentee = em.find(Rentee.class, Integer.parseInt(driverId.getId()));

            // If non existent throw exception
            if (rentee == null) {
                String errorMessage = "The user id " + driverId.getId() + " does not exist.";
                LOGGER.log(Level.WARN, errorMessage);
                throw new SearchBookingException(errorMessage);
            }

            Collection<Booking> renteeListOfBookings = rentee.getBookingCollection();

            for (Booking booking : renteeListOfBookings) {

                Car car = booking.getCar();
                CarType ct = car.getCarType();
                CarTypeDetails ctd = new CarTypeDetails(
                        Integer.toString(ct.getCarTypeId()),
                        ct.getName(),
                        ct.getNumberOfSeats(),
                        ct.getPrice().doubleValue()
                );

                BookingDetails bookingDetails = new BookingDetails(
                        booking.getPickupTime(),
                        booking.getDeliveryTime(),
                        convertStationToStationIdentifier(booking.getPickupPlace()),
                        convertStationToStationIdentifier(booking.getDeliveryPlace()),
                        new DriverDetails(Integer.toString(rentee.getRenteeId()), rentee.getName(), rentee.getDateOfBirth(), rentee.getLicenseNumber()), // TODO fix age type
                        new CarDetails(car.getLicensePlate(), ctd),
                        booking.getTotalPrice().doubleValue(),
                        booking.getBookingId() + ""
                );
                bookingList.add(bookingDetails);
            }

        } catch (Exception ex) {
            LOGGER.log(Level.ERROR, "There occured an internal server error while trying to get get a list of bookings: " + ex + "\n" + Arrays.toString(ex.getStackTrace()));
            throw new SearchBookingException("There occured an internal server error while trying to get get a list of bookings.");
        }
        LOGGER.log(Level.INFO, "There is returned a list of bookings for driver id: " + driverId.getId() + " whith the length of " + bookingList.size());

        em.close();
        return bookingList;
    }

    public BookingDetails getSpecificBooking(BookingIdentifier bookingId) throws GetSpecificBookingException, RemoteException {
        LOGGER.log(Level.INFO, "GetSpecificBooking method was called");
        // A check to see if the EntityManager has timed out - if it has, we instantiate a new one.
        if (!em.isOpen()) {
            em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
        }
        Booking booking = null;
        BookingDetails bookingDetails = null;

        try {

            try {
                booking = em.find(Booking.class, Integer.parseInt(bookingId.getBookingId()));
            } catch (Exception ex) {
                booking = null;
            }

            if (booking == null) {
                String message = "Booking with booking id :" + bookingId.getBookingId() + " does not exist";
                LOGGER.log(Level.WARN, message);
                throw new GetSpecificBookingException(message);
            }

            Rentee rentee = booking.getRentee();// POT. FIX getRenteeId returns a Rentee
            Car car = booking.getCar();
            CarType ct = car.getCarType(); // POT. FIX getCarTypeID returns a CarType
            CarTypeDetails ctd = new CarTypeDetails(
                    Integer.toString(ct.getCarTypeId()),
                    ct.getName(),
                    ct.getNumberOfSeats(),
                    ct.getPrice().doubleValue()
            );

            bookingDetails = new BookingDetails(
                    booking.getPickupTime(),
                    booking.getDeliveryTime(),
                    convertStationToStationIdentifier(booking.getPickupPlace()),
                    convertStationToStationIdentifier(booking.getDeliveryPlace()),
                    new DriverDetails(Integer.toString(rentee.getRenteeId()), rentee.getName(), rentee.getDateOfBirth(), rentee.getLicenseNumber()), // TODO fix age type
                    new CarDetails(car.getLicensePlate(), ctd),
                    booking.getTotalPrice().doubleValue(),
                    bookingId.getBookingId()
            );

        } catch (Exception ex) {
            LOGGER.log(Level.ERROR, "There occured an internal server error while trying to get the specific booking: " + ex + "\n" + Arrays.toString(ex.getStackTrace()));
            throw new GetSpecificBookingException("There occured an internal server error while trying to get the specific booking.");
        }
        return bookingDetails;
    }

    public void cancelBooking(BookingIdentifier booking) throws RemoteException {
        LOGGER.log(Level.INFO, "CancelBooking method was called");
        // A check to see if the EntityManager has timed out - if it has, we instantiate a new one.
        if (!em.isOpen()) {
            em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
        }
        EntityTransaction transaction = null;
        try {
            Booking b = em.find(Booking.class, Integer.parseInt(booking.getBookingId()));

            // If no booking is found with provided id
            if (b == null) {
                throw new CancellationException("This booking does not exist");
            }

            transaction = em.getTransaction();
            transaction.begin();
            em.remove(b);
            transaction.commit();
//            em.flush();

        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.log(Level.ERROR, "There occured an internal server error while trying to delete the specific booking: " + ex + "\n" + Arrays.toString(ex.getStackTrace()));
            throw new CancellationException("There occured an internal server error while trying to delete the specific booking.");
        }
        em.close();
    }

    public StationIdentifier convertStationToStationIdentifier(Station station) {
        StationIdentifier stationIdentifier = null;
        stationIdentifier = station.getCode() != null
                ? new AirportDetails(
                        Integer.toString(station.getStationId()),
                        station.getCode(),
                        station.getName(),
                        null, // When we request a specific booking, we don't care about the list of cars that the give station have.
                        station.getCityId().getName()
                ) : new HotelDetails(
                        Integer.toString(station.getStationId()),
                        station.getName(),
                        station.getAddress(),
                        station.getStarRating().floatValue(),
                        null,// When we request a specific booking, we don't care about the list of cars that the give station have.
                        station.getCityId().getName());
        return stationIdentifier;
    }
}
