/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import DTOs.AvailabilityDetails;
import DTOs.CarDetails;
import DTOs.CarTypeDetails;
import Entities.Booking;
import Entities.Car;
import exceptions.GetAvailableCarsException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CarDBFacade {

    EntityManager em;
    private static final Logger LOGGER = LogManager.getLogger(CarDBFacade.class);

    public CarDBFacade() {
        em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
    }

    public CarDBFacade(EntityManager entityManager) {
        em = entityManager;
    }

    public List<CarDetails> getAvailableCars(AvailabilityDetails ad) throws GetAvailableCarsException {
        LOGGER.log(Level.INFO, "GetAvailableCars method was called");
        // A check to see if the EntityManager has timed out - if it has, we instantiate a new one.
        if (!em.isOpen()) em = EntityManagerSingleton.entityManagerFactory.createEntityManager();
        
        List<Car> cars = null;

        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = em.getTransaction();;
            // Begin the transaction
            transaction.begin();

            // Get a List of available Cars
            Query q = em.createNamedQuery("Car.findCarByStation");
            q.setParameter("stationId", Integer.parseInt(ad.getPickUpPlace().getId()));

            cars = q.getResultList();

            // Commit the transaction
            transaction.commit();

        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // TODO handle exception more gracefully
            ex.printStackTrace();
            LOGGER.log(Level.ERROR, "There occured an internal server error while trying to get available cars: "+ex+"\n"+Arrays.toString(ex.getStackTrace()));
            throw new GetAvailableCarsException("There occured an internal server error while trying to get available cars.");
        }

        // Filter cars that are available (this could habe been done in DB but that data structure did not allow it...
        List<CarDetails> filteredCars = filterCarsByAvailability(cars, ad);

        return filteredCars;
    }

    private List<CarDetails> filterCarsByAvailability(List<Car> dbCars, AvailabilityDetails ad) throws GetAvailableCarsException {

        List<CarDetails> carDTOs = new ArrayList<>();

        // Filter cars by dates 
        dbCars.forEach((c) -> {

            // if the car has no bookings add it to the list
            if (c.getBookingCollection().isEmpty()) {
                carDTOs.add(new CarDetails(
                        c.getLicensePlate(),
                        new CarTypeDetails(
                                c.getCarType().getCarTypeId().toString(),
                                c.getCarType().getName(),
                                c.getCarType().getNumberOfSeats(),
                                c.getCarType().getPrice().doubleValue())
                ));
            } else {
                // Run through bookings to check availability;
                boolean isAvailable = true;
                for (Booking b : c.getBookingCollection()) {

                    Date bookedPickup = b.getPickupTime();
                    Date bookedDelivery = b.getPickupTime();

                    Date availPickup = ad.getPickUpTime();
                    Date availDelivery = ad.getDeliveryTime();

                    // Check if car is booked in the interval
                    if ((availPickup.after(bookedPickup) && availPickup.before(bookedDelivery))
                            || ((availDelivery.after(bookedPickup) && availDelivery.before(bookedDelivery)))) {
                        // if one of the booking contradict the time, dont check the rest just break and continue with next car.
                        isAvailable = false;
                        break; // if one of the booking contradict the time, dont check the rest just break and continue with next car.
                    }
                }

                if (isAvailable) {
                    carDTOs.add(new CarDetails(
                            c.getLicensePlate(),
                            new CarTypeDetails(
                                    c.getCarType().getCarTypeId().toString(),
                                    c.getCarType().getName(),
                                    c.getCarType().getNumberOfSeats(),
                                    c.getCarType().getPrice().doubleValue())
                    ));
                }
            }
        }
        );

        // if no cars were found throw an exception to frontend with an error message
        if (carDTOs.isEmpty()) {
            // throw default available cars exeption that states there are none available.
            throw new GetAvailableCarsException();
        }

        return carDTOs;
    }
}
