/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author stanislavnovitski
 */
public class EntityManagerSingleton {

    static EntityManagerFactory entityManagerFactory;

    // Create an EntityManagerFactory when you start the application.
    public EntityManagerSingleton() {
        EntityManagerSingleton.entityManagerFactory = Persistence.createEntityManagerFactory("com.threecarrental_backend_jar_1.1PU");
    }

    /**
     * Create an instance with different URL (for example for tests) Example url
     * "jdbc:mysql://localhost:3306/CarRental_dev?zeroDateTimeBehavior=convertToNull"
     *
     * @param url
     */
    public EntityManagerSingleton(String url) {
        // Map of properties that will be changed
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.url", url);
        EntityManagerSingleton.entityManagerFactory = Persistence.createEntityManagerFactory("com.threecarrental_backend_jar_1.1PU", properties);
    }
}
