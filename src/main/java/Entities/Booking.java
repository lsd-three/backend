package Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Booking", catalog = "CarRental", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Booking.findAll", query = "SELECT b FROM Booking b")
    ,
    @NamedQuery(name = "Booking.findByBookingId", query = "SELECT b FROM Booking b WHERE b.bookingId = :bookingId")
    ,
    @NamedQuery(name = "Booking.findByPickupTime", query = "SELECT b FROM Booking b WHERE b.pickupTime = :pickupTime")
    ,
    @NamedQuery(name = "Booking.findByDeliveryTime", query = "SELECT b FROM Booking b WHERE b.deliveryTime = :deliveryTime")
    ,
    @NamedQuery(name = "Booking.findByTotalPrice", query = "SELECT b FROM Booking b WHERE b.totalPrice = :totalPrice")
    ,
    @NamedQuery(name = "Booking.deleteById", query = "DELETE FROM Booking b WHERE b.bookingId = :bookingId")})

public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BookingId")
    private Integer bookingId;
    @Basic(optional = false)
    @Column(name = "PickupTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickupTime;
    @Basic(optional = false)
    @Column(name = "DeliveryTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryTime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TotalPrice")
    private BigDecimal totalPrice;
    @JoinColumn(name = "PickupPlace", referencedColumnName = "StationId")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Station pickupPlace;
    @JoinColumn(name = "DeliveryPlace", referencedColumnName = "StationId")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Station deliveryPlace;
    @JoinColumn(name = "CarLicensePlate", referencedColumnName = "LicensePlate")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Car car;
    @JoinColumn(name = "RenteeId", referencedColumnName = "RenteeId")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Rentee rentee;

    public Booking() {
    }

    public Booking(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Booking(Integer bookingId, Date pickupTime, Date deliveryTime) {
        this.bookingId = bookingId;
        this.pickupTime = pickupTime;
        this.deliveryTime = deliveryTime;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Station getPickupPlace() {
        return pickupPlace;
    }

    public void setPickupPlace(Station pickupPlace) {
        this.pickupPlace = pickupPlace;
    }

    public Station getDeliveryPlace() {
        return deliveryPlace;
    }

    public void setDeliveryPlace(Station deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Rentee getRentee() {
        return rentee;
    }

    public void setRentee(Rentee rentee) {
        this.rentee = rentee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingId != null ? bookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Booking)) {
            return false;
        }
        Booking other = (Booking) object;
        if ((this.bookingId == null && other.bookingId != null) || (this.bookingId != null && !this.bookingId.equals(other.bookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Booking[ bookingId=" + bookingId + " ]";
    }

}
