package Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "CarType", catalog = "CarRental", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarType.findAll", query = "SELECT c FROM CarType c"),
    @NamedQuery(name = "CarType.findByCarTypeId", query = "SELECT c FROM CarType c WHERE c.carTypeId = :carTypeId"),
    @NamedQuery(name = "CarType.findByName", query = "SELECT c FROM CarType c WHERE c.name = :name"),
    @NamedQuery(name = "CarType.findByNumberOfSeats", query = "SELECT c FROM CarType c WHERE c.numberOfSeats = :numberOfSeats"),
    @NamedQuery(name = "CarType.findByPrice", query = "SELECT c FROM CarType c WHERE c.price = :price")})
public class CarType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CarTypeId")
    private Integer carTypeId;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "NumberOfSeats")
    private int numberOfSeats;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "Price")
    private BigDecimal price;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carType", fetch = FetchType.EAGER)
    private Collection<Car> carCollection;

    public CarType() {
    }

    public CarType(Integer carTypeId) {
        this.carTypeId = carTypeId;
    }

    public CarType(Integer carTypeId, String name, int numberOfSeats, BigDecimal price) {
        this.carTypeId = carTypeId;
        this.name = name;
        this.numberOfSeats = numberOfSeats;
        this.price = price;
    }

    public Integer getCarTypeId() {
        return carTypeId;
    }

    public void setCarTypeId(Integer carTypeId) {
        this.carTypeId = carTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @XmlTransient
    public Collection<Car> getCarCollection() {
        return carCollection;
    }

    public void setCarCollection(Collection<Car> carCollection) {
        this.carCollection = carCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carTypeId != null ? carTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarType)) {
            return false;
        }
        CarType other = (CarType) object;
        if ((this.carTypeId == null && other.carTypeId != null) || (this.carTypeId != null && !this.carTypeId.equals(other.carTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.CarType[ carTypeId=" + carTypeId + " ]";
    }

}
