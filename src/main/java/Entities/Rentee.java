package Entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Rentee", catalog = "CarRental", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rentee.findAll", query = "SELECT r FROM Rentee r"),
    @NamedQuery(name = "Rentee.findByRenteeId", query = "SELECT r FROM Rentee r WHERE r.renteeId = :renteeId"),
    @NamedQuery(name = "Rentee.findByName", query = "SELECT r FROM Rentee r WHERE r.name = :name"),
    @NamedQuery(name = "Rentee.findByDateOfBirth", query = "SELECT r FROM Rentee r WHERE r.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "Rentee.findByLicenseNumber", query = "SELECT r FROM Rentee r WHERE r.licenseNumber = :licenseNumber")})
public class Rentee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RenteeId")
    private Integer renteeId;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "DateOfBirth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Basic(optional = false)
    @Column(name = "LicenseNumber")
    private String licenseNumber;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "rentee", fetch = FetchType.EAGER)
    private Collection<Booking> bookingCollection;

    public Rentee() {
    }

    public Rentee(Integer renteeId) {
        this.renteeId = renteeId;
    }

    public Rentee(Integer renteeId, String name, Date dateOfBirth, String licenseNumber) {
        this.renteeId = renteeId;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.licenseNumber = licenseNumber;
    }

    public Integer getRenteeId() {
        return renteeId;
    }

    public void setRenteeId(Integer renteeId) {
        this.renteeId = renteeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    @XmlTransient
    public Collection<Booking> getBookingCollection() {
        return bookingCollection;
    }

    public void setBookingCollection(Collection<Booking> bookingCollection) {
        this.bookingCollection = bookingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (renteeId != null ? renteeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rentee)) {
            return false;
        }
        Rentee other = (Rentee) object;
        if ((this.renteeId == null && other.renteeId != null) || (this.renteeId != null && !this.renteeId.equals(other.renteeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Rentee[ renteeId=" + renteeId + " ]";
    }

}
