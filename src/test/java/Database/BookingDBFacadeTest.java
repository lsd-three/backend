package Database;

import DTOs.Identifiers.*;
import exceptions.GetSpecificBookingException;
import exceptions.SearchBookingException;
import java.io.File;
import java.rmi.RemoteException;
import java.util.concurrent.CancellationException;
import javax.persistence.EntityManager;
import DTOs.BookingDetails;
import DTOs.*;
import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.NewBookingDetails;
import Entities.CarType;
import Entities.Station;
import exceptions.AddNewBookingException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.h2.tools.RunScript;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

public class BookingDBFacadeTest {

    static EntityManager em;
    static ClassLoader classLoader;
    BookingDBFacade instance;

    @Before
    public void setUp() throws ClassNotFoundException, FileNotFoundException, SQLException {
        String resourceNameCreateTables = "CarRental_CreateTables.sql";
        String resourceNameInsertData = "DummyData.sql";

        classLoader = new BookingDBFacadeTest().getClass().getClassLoader();

        // get absolute paths
        File fileCreateScript = new File(classLoader.getResource(resourceNameCreateTables).getFile());
        String createScriptAbsolutePath = fileCreateScript.getAbsolutePath();
        File fileDummyDataScript = new File(classLoader.getResource(resourceNameInsertData).getFile());
        String dummyDataScriptAbsolutePath = fileDummyDataScript.getAbsolutePath();

        // commands for loading db scripts
        Class.forName("org.h2.Driver");
        Connection _conn = DriverManager.getConnection("jdbc:h2:mem:CarRental", "root", "soft2019Backend");

        RunScript.execute(_conn, new FileReader(createScriptAbsolutePath));
        RunScript.execute(_conn, new FileReader(dummyDataScriptAbsolutePath));

        em = new EntityManagerSingleton("jdbc:h2:mem:CarRental;").entityManagerFactory.createEntityManager();
        instance = new BookingDBFacade(em);
    }

    /**
     * Test of addNewBooking method, of class BookingDBFacade.
     */
    @Test
    public void testAddNewBookingSuccess() throws AddNewBookingException, RemoteException {
        System.out.println("testAddNewBookingSuccess");

        NewBookingDetails newBookingDetails = new NewBookingDetails();
        // actual date: 2019-11-4
        Date pickupDate = new Date(2019 - 1900, 10, 4);
        // actual date: 2019-11-17
        Date deliveryDate = new Date(2019 - 1900, 10, 17);
        DriverDetails driverDetails = new DriverDetails("2", "Read Heatlie", new Date(1971 - 1900, 9, 27), "38-2669174");
        CarDetails carDetails = new CarDetails("0456895671", new CarTypeDetails("1", "A", 3, 992.1));

        newBookingDetails.setPickUpTime(pickupDate);
        newBookingDetails.setDeliveryTime(deliveryDate);
        newBookingDetails.setPickUpPlace(new StationIdentifier("1") {
        });
        newBookingDetails.setDeliveryPlace(new StationIdentifier("1") {
        });
        newBookingDetails.setDriver(driverDetails);
        newBookingDetails.setCar(carDetails);

        BookingDetails result = instance.addNewBooking(newBookingDetails);

        BookingDetails temp = instance.getSpecificBooking(new BookingIdentifier(result.getBookingId()) {
        });
        assertEquals("6", temp.getBookingId());

        assertNotEquals(null, result);
        assertEquals("6", result.getBookingId());
        assertEquals(newBookingDetails.getPickUpTime(), result.getPickUpTime());
        assertEquals(newBookingDetails.getDeliveryTime(), result.getDeliveryTime());
        assertEquals(newBookingDetails.getPickUpPlace().getId(), result.getPickUpPlace().getId());
        assertEquals(newBookingDetails.getDeliveryPlace().getId(), result.getDeliveryPlace().getId());
        assertEquals(newBookingDetails.getDriver().getId(), result.getDriver().getId());
        assertEquals(newBookingDetails.getCar().getId(), result.getCar().getId());

    }

    /**
     * Test of addNewBooking method, of class BookingDBFacade. Creating a new
     * booking with non existing rentee
     */
    @Test
    public void testAddNewBookingNewRentee() throws AddNewBookingException, RemoteException {
        System.out.println("testAddNewBookingNewRentee");

        NewBookingDetails newBookingDetails = new NewBookingDetails();
        // actual date: 2019-11-4
        Date pickupDate = new Date(2019, 10, 4);
        // actual date: 2019-11-17
        Date deliveryDate = new Date(2019, 10, 17);
        DriverDetails driverDetails = new DriverDetails("200", "Jeff", new Date(1971, 9, 27), "38-2669172");
        CarDetails carDetails = new CarDetails("0456895671", new CarTypeDetails("1", "A", 3, 992.1));

        newBookingDetails.setPickUpTime(pickupDate);
        newBookingDetails.setDeliveryTime(deliveryDate);
        newBookingDetails.setPickUpPlace(new StationIdentifier("1") {
        });
        newBookingDetails.setDeliveryPlace(new StationIdentifier("1") {
        });
        newBookingDetails.setDriver(driverDetails);
        newBookingDetails.setCar(carDetails);

        BookingDetails result = instance.addNewBooking(newBookingDetails);

        assertNotEquals(null, result);
        assertEquals("6", result.getBookingId());
        assertEquals("6", result.getDriver().getId());

    }

    /**
     * Test of addNewBooking method, of class BookingDBFacade. Creating a new
     * booking with non existing rentee without provided ID
     */
    @Test
    public void testAddNewBookingNewRenteeNoId() throws AddNewBookingException, RemoteException {
        System.out.println("testAddNewBookingNewRenteeNoId");

        NewBookingDetails newBookingDetails = new NewBookingDetails();
        // actual date: 2019-11-4
        Date pickupDate = new Date(2019, 10, 4);
        // actual date: 2019-11-17
        Date deliveryDate = new Date(2019, 10, 17);
        DriverDetails driverDetails = new DriverDetails(null, "Jeff", new Date(1971, 9, 27), "38-2669172");
        CarDetails carDetails = new CarDetails("0456895671", new CarTypeDetails("1", "A", 3, 992.1));

        newBookingDetails.setPickUpTime(pickupDate);
        newBookingDetails.setDeliveryTime(deliveryDate);
        newBookingDetails.setPickUpPlace(new StationIdentifier("1") {
        });
        newBookingDetails.setDeliveryPlace(new StationIdentifier("1") {
        });
        newBookingDetails.setDriver(driverDetails);
        newBookingDetails.setCar(carDetails);

        BookingDetails result = instance.addNewBooking(newBookingDetails);

        assertNotEquals(null, result);
        assertEquals("6", result.getBookingId());
        assertEquals("6", result.getDriver().getId());

    }
    
    @Test(expected = AddNewBookingException.class)
    public void testAddNewBookingFailure() throws Exception {
        System.out.println("addNewBooking");

        NewBookingDetails newBookingDetails = null;
        BookingIdentifier expResult = null;

        BookingIdentifier result = instance.addNewBooking(newBookingDetails);
    }

    /**
     * Test of calcTotalPrice helper method, of class BookingDBFacade. This test
     * tests if the method returns a fee for 5 days 20.00 * 5 = 100.00
     */
    @Test
    public void calcTotalPriceSameDelivery() throws Exception {
        System.out.println("calcTotalPrice with same delivery (no fee)");

        double expectedResult = 100.00; // price for 5 days rental

        CarType carType = new CarType(1, "name", 4, new BigDecimal(20.00));
        Date pickupTime = new Date(2000, 1, 1);
        Date deliveryTime = new Date(2000, 1, 5);
        Station pickupPlace = new Station(1);
        Station deliveryPlace = new Station(1);

        double result = instance.calcTotalPrice(carType, pickupTime, deliveryTime, pickupPlace, deliveryPlace);

        assertEquals(expectedResult, result, 0);
    }

    /**
     * Test of calcTotalPrice helper method, of class BookingDBFacade. This test
     * tests if the method returns a fee for 5 days 20.00 * 5 = 100.00 but with
     * a fee of 50 for different delivery
     */
    @Test
    public void calcTotalPriceDiffrentDelivery() throws Exception {
        System.out.println("calcTotalPrice with diffrent delivery (50.00 fee added)");

        double expectedResult = 150.00; // price for 5 days rental

        CarType carType = new CarType(1, "name", 4, new BigDecimal(20.00));
        Date pickupTime = new Date(2000, 1, 1);
        Date deliveryTime = new Date(2000, 1, 5);
        Station pickupPlace = new Station(1);
        Station deliveryPlace = new Station(2);

        double result = instance.calcTotalPrice(carType, pickupTime, deliveryTime, pickupPlace, deliveryPlace);

        assertEquals(expectedResult, result, 0);
    }

    /**
     * Test of searchBooking method, of class BookingDBFacade.
     */
    @Test
    public void testSearchBookingSuccess() throws Exception {
        System.out.println("searchBooking");

        int expectedSize = 3;

        DriverIdentifier driver = new DriverIdentifier("1") {
        };

        List<BookingDetails> result = instance.searchBooking(driver);

        assertEquals(expectedSize, result.size());
        assertEquals(driver.getId(), result.get(0).getDriver().getId());
    }

    @Test(expected = SearchBookingException.class)
    public void testSearchBookingFailure() throws SearchBookingException, RemoteException {
        System.out.println("searchBooking");

        DriverIdentifier driver = new DriverIdentifier("10") {
        };

        instance.searchBooking(driver);
    }

    /**
     * Test of getSpecificBooking method, of class BookingDBFacade.
     */
    @Test
    public void testGetSpecificBookingSuccess() throws Exception {
        System.out.println("testGetSpecificBookingSuccess");

        BookingIdentifier bookingId = new BookingIdentifier("1") {
        };
        // actual date: 2019-11-4
        Date pickupDate = new Date(2019 - 1900, 10, 4);
        // actual date: 2019-11-17
        Date deliveryDate = new Date(2019 - 1900, 10, 17);

        BookingDetails bookingDetailsExpected = new BookingDetails(pickupDate, deliveryDate, new StationIdentifier("1") {
        }, new StationIdentifier("2") {
        }, new DriverIdentifier("1") {
        }, new CarIdentifier("0061602965") {
        }, 715.9, bookingId.getBookingId());

        BookingDetails result = instance.getSpecificBooking(bookingId);

        assertEquals(bookingDetailsExpected.getBookingId(), result.getBookingId());
        assertEquals(bookingDetailsExpected.getPickUpTime(), result.getPickUpTime());
        assertEquals(bookingDetailsExpected.getDeliveryTime(), result.getDeliveryTime());
        assertEquals(bookingDetailsExpected.getPickUpPlace().getId(), result.getPickUpPlace().getId());
        assertEquals(bookingDetailsExpected.getDeliveryPlace().getId(), result.getDeliveryPlace().getId());
        assertEquals(bookingDetailsExpected.getDriver().getId(), result.getDriver().getId());
        assertEquals(bookingDetailsExpected.getCar().getId(), result.getCar().getId());
        assertEquals(bookingDetailsExpected.getTotalPrice(), result.getTotalPrice(), 2);
    }

    @Test(expected = GetSpecificBookingException.class)
    public void testGetSpecificBookingFailure() throws GetSpecificBookingException, RemoteException {
        System.out.println("testGetSpecificBookingFailure");

        BookingIdentifier bookingId = new BookingIdentifier("10") {
        };

        instance.getSpecificBooking(bookingId);
    }

    /**
     * Test of cancelBooking method, of class BookingDBFacade.
     */
    @Test(expected = GetSpecificBookingException.class)
    public void testCancelBookingSuccess() throws Exception {
        System.out.println("testCancelBookingSuccess");

        BookingIdentifier bookingId = new BookingIdentifier("2") {
        };

        BookingDetails booking_before = instance.getSpecificBooking(bookingId);
        assertNotNull(booking_before);

        instance.cancelBooking(bookingId);

        // should throw exception if booking has been deleted
        instance.getSpecificBooking(bookingId);
    }

    @Test(expected = CancellationException.class)
    public void testCancelBookingFailure() throws Exception {
        System.out.println("cancelBooking");

        // triger error while parsing
        BookingIdentifier bookingId = new BookingIdentifier("Test") {
        };

        instance.cancelBooking(bookingId);
    }
    
}
