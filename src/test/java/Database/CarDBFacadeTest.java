package Database;

import DTOs.AvailabilityDetails;
import DTOs.CarDetails;
import DTOs.Identifiers.StationIdentifier;
import exceptions.GetAvailableCarsException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import javax.persistence.EntityManager;
import org.junit.Before;
import java.util.List;
import org.h2.tools.RunScript;
import org.junit.Test;
import static org.junit.Assert.*;

public class CarDBFacadeTest {
    
    static EntityManager em;
    static ClassLoader classLoader;
    CarDBFacade instance;
    
    @Before
    public void setUp() throws FileNotFoundException, SQLException, ClassNotFoundException{
        String resourceNameCreateTables = "CarRental_CreateTables.sql";
        String resourceNameInsertData = "DummyData.sql";

        classLoader = new BookingDBFacadeTest().getClass().getClassLoader();

        // get absolute paths
        File fileCreateScript = new File(classLoader.getResource(resourceNameCreateTables).getFile());
        String createScriptAbsolutePath = fileCreateScript.getAbsolutePath();
        File fileDummyDataScript = new File(classLoader.getResource(resourceNameInsertData).getFile());
        String dummyDataScriptAbsolutePath = fileDummyDataScript.getAbsolutePath();

        // commands for loading db scripts
        Class.forName("org.h2.Driver");
        Connection _conn = DriverManager.getConnection("jdbc:h2:mem:CarRental", "root", "soft2019Backend");

        RunScript.execute(_conn, new FileReader(createScriptAbsolutePath));
        RunScript.execute(_conn, new FileReader(dummyDataScriptAbsolutePath));

        em = new EntityManagerSingleton("jdbc:h2:mem:CarRental;").entityManagerFactory.createEntityManager();
        instance = new CarDBFacade(em);
    }
    
    @Test
    public void testGetAvailableCarsSuccess() {
        System.out.println("getAvailableCars");
        
        Date pickupDate = new Date(2019, 11, 4);
        Date deliveryDate = new Date(2019, 11, 17);
        
        AvailabilityDetails ad = new AvailabilityDetails(pickupDate, deliveryDate, new StationIdentifier("1"){});
        
        List<CarDetails> result = instance.getAvailableCars(ad);
        
        assertEquals(1, result.size());
    }
    
    @Test(expected = GetAvailableCarsException.class)
    public void testGetAvailableCarsFailure() {
        System.out.println("getAvailableCars");
        
        AvailabilityDetails ad = null;
        
        List<CarDetails> result = instance.getAvailableCars(ad);
    }
}
