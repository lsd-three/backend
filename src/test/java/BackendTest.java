
import DTOs.HotelDetails;
import DTOs.Identifiers.CarIdentifier;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.h2.tools.RunScript;
import static org.junit.Assert.*;
import org.junit.*;

public class BackendTest {

    Connection _conn;

    @Before
    public void setUp() throws ClassNotFoundException, SQLException, FileNotFoundException {
        String resourceNameCreateTables = "CarRental_CreateTables.sql";
        String resourceNameInsertData = "DummyData.sql";

        ClassLoader classLoader = getClass().getClassLoader();

        // get absolute paths
        File fileCreateScript = new File(classLoader.getResource(resourceNameCreateTables).getFile());
        String createScriptAbsolutePath = fileCreateScript.getAbsolutePath();
        File fileDummyDataScript = new File(classLoader.getResource(resourceNameInsertData).getFile());
        String dummyDataScriptAbsolutePath = fileDummyDataScript.getAbsolutePath();

        Class.forName("org.h2.Driver");
        _conn = DriverManager.getConnection("jdbc:h2:mem:carRental", "root", "");

        RunScript.execute(_conn, new FileReader(createScriptAbsolutePath));
        RunScript.execute(_conn, new FileReader(dummyDataScriptAbsolutePath));
    }

    @After
    public void tearDown() throws SQLException {
        _conn.close();
    }

    @Test
    public void testGetCity() throws SQLException {
        Statement cityTableStatement = _conn.createStatement();

        ResultSet rset = cityTableStatement.executeQuery("SELECT * FROM City;");

        rset.first();
        // get values by column number
        int cityId1 = rset.getInt(1);
        String cityName1 = rset.getString(2);

        assertEquals(1, cityId1);
        assertEquals("Tuquan", cityName1);

        // move iterator to the next result
        rset.next();
        int cityId2 = rset.getInt(1);
        String cityName2 = rset.getString(2);

        assertEquals(2, cityId2);
        assertEquals("Junqueiro", cityName2);
    }

    @Test
    public void testGetRentee() throws SQLException {
        Statement renteeTableStatement = _conn.createStatement();

        ResultSet rset = renteeTableStatement.executeQuery("SELECT * FROM Rentee WHERE RenteeId = 2;");

        rset.first();
        // get values by column number
        int renteeId1 = rset.getInt(1);
        String renteeLicenseNumber1 = rset.getString(4);

        assertEquals(2, renteeId1);
        assertEquals("38-2669174", renteeLicenseNumber1);
    }

    @Test
    public void testGetCount() throws SQLException {
        Statement renteeTableStatement = _conn.createStatement();

        ResultSet rset = renteeTableStatement.executeQuery("SELECT COUNT(*) FROM City;");

        rset.first();
        int resultCount = rset.getInt(1);

        assertEquals(5, resultCount);
    }

    @Test
    public void testDelete() throws SQLException {
        Statement cityTableStatement = _conn.createStatement();

        ResultSet rsetCountBefore = cityTableStatement.executeQuery("SELECT COUNT(*) FROM City;");
        rsetCountBefore.first();
        assertEquals(5, rsetCountBefore.getInt(1));

        cityTableStatement.executeUpdate("DELETE FROM City WHERE CityId = 1;");

        ResultSet rsetCountAfter = cityTableStatement.executeQuery("SELECT COUNT(*) FROM City;");
        rsetCountAfter.first();
        assertEquals(4, rsetCountAfter.getInt(1));
    }

    @Test
    public void testCountAfterDelete() throws SQLException {
        Statement cityTableStatement = _conn.createStatement();

        ResultSet rset = cityTableStatement.executeQuery("SELECT COUNT(*) FROM City;");
        rset.first();
        assertEquals(5, rset.getInt(1));
    }

    // Test DTO
    @Test
    public void testGetHotelName() {
        List<CarIdentifier> list = new ArrayList<CarIdentifier>();
        String nameOfHotel = new HotelDetails("1", "Some hotel", "some address", 2.2f, list, "Some city").getName();
        assertEquals("Some hotel", nameOfHotel);
    }
}
