USE CarRental;

INSERT INTO `City` VALUES (1,'Tuquan'),(2,'Junqueiro'),(3,'Xinminxiang'),(4,'Nueva Gerona'),(5,'Kadusirung Hilir');

INSERT INTO `Rentee` VALUES (1,'Gretta Thake','1966-10-18','22-9484786'),(2,'Read Heatlie','1971-10-27','38-2669174'),(3,'Farrand Petican','1968-02-08','81-8711694'),(4,'Tamarah Frankum','1999-01-18','31-5042136'),(5,'Sonny Dible','1974-01-16','82-3018863');

INSERT INTO `Station` VALUES (1,'63629-1748','Airport','Bushpig','8 Harbort Park',4.4,2),(2,'65044-6585','Hotel','Oriental short-clawed otter','88 Lunder Circle',4.1,2),(3,'49643-114','Airport','Goose, snow','2815 Kensington Trail',1.6,3),(4,'54868-3041','Airport','Small-clawed otter','67843 Talmadge Place',2.2,4),(5,'57337-024','Hotel','Mynah, indian','847 Hanover Trail',4.5,5);

INSERT INTO `CarType` VALUES (1,'A',3,992.10),(2,'B',5,487.51),(3,'C',2,119.58),(4,'D',9,260.81),(5,'E',3,698.33);

INSERT INTO `Car` VALUES ('0061602965',1,1),('0215601483',2,2),('0361651236',3,3),('0456895671',4,4),('0530373149',5,5);

INSERT INTO `Booking` VALUES (1,'2019-11-04', '2019-11-17', 715.9, 1, 2, '0061602965', 1),(2,'2019-11-05', '2019-11-18', 818.24, 2, 3, '0215601483', 1),(3,'2019-11-14', '2019-11-19', 905.39, 3, 4, '0361651236', 1),(4,'2019-11-12', '2019-11-18', 719.12, 4, 5, '0456895671', 5),(5,'2019-11-06', '2019-11-16', 598.01, 5, 1, '0530373149', 5);